<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:template match='/'>
		<xsl:apply-templates select='regions/region' mode='file' />
		<xsl:result-document href='index.html'>
			<html>
				<head>
					<meta charset="utf-8"></meta>
					<title>Index</title>
					<link rel="stylesheet" type="text/css" media="screen" href="html/style.css" ></link>
				</head>
				<body>
					<div class="parent">
						<div class="leftIndex"></div>
						<div class="rightIndex"></div>
						<div class="bottomIndex"></div>
						<div class="centerIndex"><ul><xsl:apply-templates select='regions/region' mode='index'/></ul></div>
						<div class="topIndex"><h1>Regions list</h1></div>
					</div>
				</body>
			</html>
		</xsl:result-document>
	</xsl:template>
	<xsl:template match='region' mode='index'>
		<li>
			<a href='regions/{@name}.html'>
				<span><xsl:value-of select='./@name'></xsl:value-of></span>
			</a>
		</li>
	</xsl:template>
	<xsl:template match='region' mode='file'>
        <xsl:result-document href='regions/{@name}.html'>
            <html>
                <head>
                    <title>
                        <xsl:value-of select='@name'></xsl:value-of>
                    </title>
                    <meta charset="utf-8"></meta>
                    <link rel="stylesheet" type="text/css" media="screen" href="../html/style.css"></link>
                </head>
                <body>
                    <div class='parent'>
						<div class="centerRegion"><xsl:apply-templates select='section' mode='content'></xsl:apply-templates></div>
						<div class="topRegion"></div>
						<div class="bottomRegion"></div>
						<div class="leftRegion"><ul class='menu'>
<li class='menuElement'><a href='../index.html'>Start page</a></li>
<xsl:apply-templates select='section' mode='menu'></xsl:apply-templates>
</ul>
</div>
						<div class="rightRegion"><img class='flag' src='../html/{@name}.gif'></img></div>
                        <div class='topRegion'><h1><xsl:value-of select='@name'></xsl:value-of></h1></div>                    </div>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match='section' mode='menu'>
        <li class='sectionElement'><a href='#{@name}'> <xsl:value-of select='@name'></xsl:value-of></a></li>
        <ul class='category'>
            <xsl:apply-templates select='category' mode='menu'></xsl:apply-templates>
        </ul>
    </xsl:template>

    <xsl:template match='category' mode='menu'>
        <li class='categoryElement'><a href='#{@name}'> - <xsl:value-of select='@name'></xsl:value-of> </a></li>
    </xsl:template>
    <xsl:template match='section' mode='content'>
        <section id='{@name}'>
            <h2>
                <xsl:value-of select='@name'></xsl:value-of>
            </h2>
            <xsl:apply-templates select='category' mode='content'></xsl:apply-templates>
        </section>
    </xsl:template>

    <xsl:template match='category' mode='content'>
        <div id='{@name}'>
            <h3>
                <xsl:value-of select='@name'></xsl:value-of>
            </h3>
            <xsl:apply-templates select='data' mode='content'></xsl:apply-templates>
        </div>
    </xsl:template>

    <xsl:template match='data' mode='content'>
        <xsl:choose>
            <xsl:when test="@name">
                <p>
                    <b><xsl:value-of select='@name'></xsl:value-of></b>
                    <xsl:text>: </xsl:text>
                    <xsl:value-of select='.'></xsl:value-of>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <p><xsl:value-of select='.'></xsl:value-of></p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
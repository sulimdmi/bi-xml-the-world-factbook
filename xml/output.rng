<grammar xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
	<start>
		<element name="regions">
			<oneOrMore>
				<ref name="region"/>
			</oneOrMore>
		</element>
	</start>
	<define name="region">
		<element name="region">
			<group>
				<attribute name="id">
					<choice>
						<value>holy see (vatican city)</value>
						<value>monaco</value>
						<value>nauru</value>
						<value>tuvalu</value>
					</choice>
				</attribute>
				<attribute name="name">
					<choice>
						<value>holy see (vatican city)</value>
						<value>monaco</value>
						<value>nauru</value>
						<value>tuvalu</value>
					</choice>
				</attribute>
			</group>
			<zeroOrMore>
				<ref name="section"/>
			</zeroOrMore>
		</element>
	</define>
	<define name="section">
		<element name="section">
			<choice>
				<group>
					<attribute name="name"><value>Introduction</value></attribute>
					<zeroOrMore>
						<ref name="Introduction"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Geography</value></attribute>
					<zeroOrMore>
						<ref name="Geography"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>People and Society</value></attribute>
					<zeroOrMore>
						<ref name="PeopleandSociety"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Government</value></attribute>
					<zeroOrMore>
						<ref name="Government"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Economy</value></attribute>
					<zeroOrMore>
						<ref name="Economy"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Communications</value></attribute>
					<zeroOrMore>
						<ref name="Communications"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Military and Security</value></attribute>
					<zeroOrMore>
						<ref name="MilitaryandSecurity"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Transportation</value></attribute>
					<zeroOrMore>
						<ref name="Transportation"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Transnational Issues</value></attribute>
					<zeroOrMore>
						<ref name="TransnationalIssues"/>
					</zeroOrMore>
				</group>
				<group>
					<attribute name="name"><value>Energy</value></attribute>
					<zeroOrMore>
						<ref name="Energy"/>
					</zeroOrMore>
				</group>
			</choice>
		</element>
	</define>
<define name="Introduction">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Background</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="Geography">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Location</value>
				<value>Geographic coordinates</value>
				<value>Map references</value>
				<value>Area</value>
				<value>Area - comparative</value>
				<value>Land boundaries</value>
				<value>Coastline</value>
				<value>Maritime claims</value>
				<value>Climate</value>
				<value>Terrain</value>
				<value>Elevation</value>
				<value>Natural resources</value>
				<value>Land use</value>
				<value>Natural hazards</value>
				<value>Environment - current issues</value>
				<value>Environment - international agreements</value>
				<value>Geography - note</value>
				<value>Irrigated land</value>
				<value>Population distribution</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="PeopleandSociety">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Population</value>
				<value>Nationality</value>
				<value>Ethnic groups</value>
				<value>Languages</value>
				<value>Religions</value>
				<value>Population growth rate</value>
				<value>Urbanization</value>
				<value>Major urban areas - population</value>
				<value>HIV/AIDS - adult prevalence rate</value>
				<value>HIV/AIDS - people living with HIV/AIDS</value>
				<value>HIV/AIDS - deaths</value>
				<value>Education expenditures</value>
				<value>Age structure</value>
				<value>Median age</value>
				<value>Birth rate</value>
				<value>Death rate</value>
				<value>Net migration rate</value>
				<value>Population distribution</value>
				<value>Sex ratio</value>
				<value>Infant mortality rate</value>
				<value>Life expectancy at birth</value>
				<value>Total fertility rate</value>
				<value>Drinking water source</value>
				<value>Current Health Expenditure</value>
				<value>Physicians density</value>
				<value>Hospital bed density</value>
				<value>Sanitation facility access</value>
				<value>Unemployment, youth ages 15-24</value>
				<value>Obesity - adult prevalence rate</value>
				<value>School life expectancy (primary to tertiary education)</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="Government">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Country name</value>
				<value>Government type</value>
				<value>Capital</value>
				<value>Administrative divisions</value>
				<value>Independence</value>
				<value>National holiday</value>
				<value>Constitution</value>
				<value>Legal system</value>
				<value>International law organization participation</value>
				<value>Citizenship</value>
				<value>Suffrage</value>
				<value>Executive branch</value>
				<value>Legislative branch</value>
				<value>Judicial branch</value>
				<value>Political parties and leaders</value>
				<value>International organization participation</value>
				<value>Diplomatic representation in the US</value>
				<value>Diplomatic representation from the US</value>
				<value>Flag description</value>
				<value>National symbol(s)</value>
				<value>National anthem</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="Economy">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Economy - overview</value>
				<value>GDP (purchasing power parity)</value>
				<value>Industries</value>
				<value>Labor force</value>
				<value>Labor force - by occupation</value>
				<value>Population below poverty line</value>
				<value>Budget</value>
				<value>Taxes and other revenues</value>
				<value>Budget surplus (+) or deficit (-)</value>
				<value>Fiscal year</value>
				<value>Exchange rates</value>
				<value>GDP (official exchange rate)</value>
				<value>GDP - real growth rate</value>
				<value>GDP - per capita (PPP)</value>
				<value>GDP - composition, by sector of origin</value>
				<value>Agriculture - products</value>
				<value>Industrial production growth rate</value>
				<value>Unemployment rate</value>
				<value>Household income or consumption by percentage share</value>
				<value>Inflation rate (consumer prices)</value>
				<value>Market value of publicly traded shares</value>
				<value>Exports</value>
				<value>Imports</value>
				<value>Debt - external</value>
				<value>GDP - composition, by end use</value>
				<value>Public debt</value>
				<value>Current account balance</value>
				<value>Exports - partners</value>
				<value>Exports - commodities</value>
				<value>Imports - commodities</value>
				<value>Imports - partners</value>
				<value>Commercial bank prime lending rate</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="Communications">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Telephone system</value>
				<value>Broadcast media</value>
				<value>Internet country code</value>
				<value>Communications - note</value>
				<value>Telephones - fixed lines</value>
				<value>Telephones - mobile cellular</value>
				<value>Internet users</value>
				<value>Broadband - fixed subscriptions</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="MilitaryandSecurity">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Military and security forces</value>
				<value>Military service age and obligation</value>
				<value>Military - note</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="Transportation">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Railways</value>
				<value>Civil aircraft registration country code prefix</value>
				<value>Heliports</value>
				<value>Roadways</value>
				<value>Ports and terminals</value>
				<value>National air transport system</value>
				<value>Airports</value>
				<value>Airports - with paved runways</value>
				<value>Airports - with unpaved runways</value>
				<value>Merchant marine</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="TransnationalIssues">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Disputes - international</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
<define name="Energy">
	<element name="category">
		<attribute name="name">
			<choice>
				<value>Electricity access</value>
				<value>Electricity - production</value>
				<value>Electricity - consumption</value>
				<value>Electricity - exports</value>
				<value>Electricity - imports</value>
				<value>Electricity - installed generating capacity</value>
				<value>Electricity - from fossil fuels</value>
				<value>Electricity - from nuclear fuels</value>
				<value>Electricity - from hydroelectric plants</value>
				<value>Electricity - from other renewable sources</value>
				<value>Crude oil - production</value>
				<value>Crude oil - exports</value>
				<value>Crude oil - imports</value>
				<value>Crude oil - proved reserves</value>
				<value>Refined petroleum products - production</value>
				<value>Refined petroleum products - consumption</value>
				<value>Refined petroleum products - exports</value>
				<value>Refined petroleum products - imports</value>
				<value>Natural gas - production</value>
				<value>Natural gas - consumption</value>
				<value>Natural gas - exports</value>
				<value>Natural gas - imports</value>
				<value>Natural gas - proved reserves</value>
				<value>Carbon dioxide emissions from consumption of energy</value>
			</choice>
		</attribute>
		<zeroOrMore>
			<ref name="data"/>
		</zeroOrMore>
	</element>
</define>
	<define name="data">
		<element name="data">
			<text/>
			<optional>
				<attribute name="name">
					<text/>
				</attribute>
			</optional>
		</element>
	</define>
</grammar>